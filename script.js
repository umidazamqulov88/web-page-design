const items = [
    { id: 1, category: "electronics", title: "Smartphone", description: "A powerful smartphone with advanced features.", image: "smartphone.jpg" },
    { id: 1, category: "electronics", title: "Smartphone", description: "A powerful smartphone with advanced features.", image: "smartphone.jpg" },
    { id: 2, category: "books", title: "Book Title", description: "An interesting book that you should read.", image: "book.jpg" },
    { id: 2, category: "books", title: "Book Title", description: "An interesting book that you should read.", image: "book.jpg" },
  ];
  
  function renderItems() {
    const container = document.getElementById("itemContainer");
    container.innerHTML = "";
    items.forEach(item => {
      const itemElement = document.createElement("section");
      itemElement.className = "item";
      itemElement.innerHTML = `
        <img src="${item.image}" alt="${item.title}">
        <h2>${item.title}</h2>
        <p>${item.description}</p>
      `;
      itemElement.addEventListener("click", () => showDetails(item.id));
      container.appendChild(itemElement);
    });
  }
  
  function showDetails(itemId) {
    const selectedItem = items.find(item => item.id === itemId);
    const detailsContainer = document.createElement("div");
    detailsContainer.className = "details";
    detailsContainer.innerHTML = `
      <h2>${selectedItem.title}</h2>
      <p>${selectedItem.description}</p>
      <button onclick="hideDetails()">Close Details</button>
    `;
    document.body.appendChild(detailsContainer);
  }
  
  function hideDetails() {
    const detailsContainer = document.querySelector(".details");
    if (detailsContainer) {
      detailsContainer.remove();
    }
  }
  
  function applyFilters() {
    const searchValue = document.getElementById("search").value.toLowerCase();
    const filterValue = document.getElementById("filter").value.toLowerCase();
  
    const filteredItems = items.filter(item => {
      const titleMatch = item.title.toLowerCase().includes(searchValue);
      const categoryMatch = filterValue === "all" || item.category === filterValue;
      return titleMatch && categoryMatch;
    });
  
    items.forEach(item => {
      const itemElement = document.querySelector(`.item[data-id="${item.id}"]`);
      if (filteredItems.includes(item)) {
        itemElement.style.display = "block";
      } else {
        itemElement.style.display = "none";
      }
    });
  }
  
  renderItems();
  